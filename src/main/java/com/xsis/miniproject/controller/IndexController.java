package com.xsis.miniproject.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {
	@GetMapping("/")
	public String showIndex() {
		return "index";
	}
	
	@GetMapping("category/")
	public String showCategory() {
		return "/modules/category/category.html";
	}
	
	@GetMapping("variant/")
	public String showVariant() {
		return "/modules/variant/variant.html";
	}
	
	@GetMapping("product/")
	public String showProduct() {
		return "/modules/product/product.html";
	}
	
	@GetMapping("order/")
	public String showOrder() {
		return "/modules/order/order.html";
	}
	
	@GetMapping("order/modal")
	public String showModal() {

		return "/modules/order/modal-order.html";
	}

}
